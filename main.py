import pandas as pd
import copy
import re
import sys
import openpyxl
from fuzzywuzzy import fuzz
from multiprocessing.dummy import Pool as ThreadPool
import time

tic = time.time()

origin_df = pd.DataFrame(columns=['Населенный Пункт', 'Улица', 'Дом', 'Название', 'Исходный Индекс'])
working_df = pd.DataFrame()
duplicate_df = pd.DataFrame()
choice_df = pd.DataFrame(columns=['Choice'])

min_likenesses = dict({'Населенный Пункт': 85,
                       'Улица': 85,
                       'Дом': 100,
                       'Название': 90})

thread_count = 4

df_dict = pd.read_excel("data.xlsx", sheet_name=None,
                        converters={'Населенный Пункт': str, 'Улица': str, 'Дом': str, 'Название': str})
# df_dict = pd.read_excel("test2.xlsx", sheet_name=None,
#                         converters={'Населенный Пункт': str, 'Улица': str, 'Дом': str, 'Название': str})


# выводит на экран брэйкер
def breaker():
    print('\n', "============================== BREAKER ==============================", '\n')


# заполняет working_df необходимыми данными из всех таблиц
def filling_origin_df(dict_of_df: dict) -> pd.DataFrame:
    global origin_df

    for sheet_key in dict_of_df:
        current_df = dict_of_df[sheet_key].loc[:, ['Населенный Пункт', 'Улица', 'Дом', 'Название']]
        n = current_df.shape[0]
        ind = pd.Series((sheet_key, num + 2) for num in range(n))
        current_df = current_df.assign(**{'Исходный Индекс': ind})
        origin_df = origin_df.append(current_df, ignore_index=True)

    return origin_df


# подготавливаем working_df
def data_cleaner() -> pd.DataFrame:
    global working_df

    for key in working_df:
        if key == 'Исходный Индекс':
            break
        # замена ё на е
        working_df[key] = working_df[key].str.replace(r'ё', 'е')
        working_df[key] = working_df[key].str.replace(r'Ё', 'Е')
        # удаление слова "улица" и прочих символов
        working_df[key] = working_df[key].str.replace(r',|\"|\'|| *улица *', '')
        # замена непечатаемых символов на пробел
        working_df[key] = working_df[key].str.replace(r'[^\w\-\(\)0-9\.\\/#№&]|[ +]', r' ')
        # заменяем NaN на empty чтобы можно было нормально работать
        working_df[key] = working_df[key].replace(pd.np.nan, 'empty', regex=True)

    return working_df


# оставляет от объектов только их названия
def stupid_worker() -> pd.DataFrame:
    global working_df, duplicate_df, choice_df, min_likenesses, thread_count

    # тупо удаляем лишнее, оставляем только названия
    def stupid_cleaner() -> pd.DataFrame:
        global working_df

        for key in working_df:
            if key == 'Исходный Индекс':
                break

            if key == 'Населенный Пункт':
                working_df[key] = working_df[key].replace(pd.np.nan, 'empty', regex=True)
                for ind in range(len(working_df[key])):
                    if working_df[key][ind] == 'empty':
                        working_df[key][ind] = 'empty'
                        continue

                    name = re.findall(r'[А-ЯA-ZЁ0-9][а-яёa-z\- \(\)А-ЯA-ZЁ0-9]*', working_df[key][ind])
                    working_df[key][ind] = name[0]
                working_df[key] = working_df[key].str.lower()

            elif key == 'Улица':
                working_df[key] = working_df[key].str.replace(
                    r',|\"|\'| *аллея *| *бульвар *| *переулок *| *площадь *| *проспект *| *улица *', r'')
                working_df[key] = working_df[key].str.lower()

            elif key == 'Дом':
                working_df[key] = working_df[key].str.replace(r' ', '')
                working_df[key] = working_df[key].str.lower()

            elif key == 'Название':
                working_df[key] = working_df[key].str.replace(
                    r',|\"|\'|\.| *ООО *| *ОАО *| *ЗАО *| *ИП *| *АО *| *ФГУП *| *МУП *| *ГУП *| *МП *| *ПАО *', r'')
                working_df[key] = working_df[key].str.lower()

        return working_df

    # стартует выполнение (разбивает working_df на подфреймы и передаёт их в worker)
    def starter():
        global working_df, thread_count
        working_df = stupid_cleaner()

        name = working_df.columns[0]
        dropable_ind = list()

        # pool = ThreadPool(thread_count)
        # df_list = list()

        for x in working_df[name].unique():
            res = working_df[
                working_df.apply(lambda row: fuzz.token_sort_ratio(str(row[name]), x), axis=1) >= min_likenesses[name]][
                working_df.columns[1:]]

            if len(res) > 1:
                # df_list.append(res)
                # if len(df_list) == thread_count:
                #     pool.map(worker, df_list)
                #     df_list.clear()
                worker(res)
            else:
                try:
                    dropable_ind.append(res.iloc[0]['Исходный Индекс'])
                except:
                    e = sys.exc_info()[1]
                    print(e.args[0], 134)
                    print(x)

        working_df = working_df[~working_df['Исходный Индекс'].isin(dropable_ind)]
        # pool.map(worker, df_list)

        return

    # ищет fuzzy дубликаты и выделяет на них новый DataFrame и отбрасывает уникальные записи из working_df
    def worker(df: pd.DataFrame):
        global origin_df, working_df, duplicate_df, choice_df

        name = df.columns[0]
        dropable_ind = list()
        df = pd.merge(df, working_df)[df.columns]

        if name == 'Исходный Индекс':
            # приоритет информации от Заказчика
            lst = [item for item in df[name] if 'Заказчик' in item[0]]
            if len(lst) != 0:
                choice = working_df[working_df['Исходный Индекс'] == lst[0]]['Исходный Индекс']
                working_df = working_df.drop(choice.index)
            else:
                choice = working_df[working_df['Исходный Индекс'] == df[name][0]]['Исходный Индекс']
                working_df = working_df.drop(choice.index)

            # создаём таблицу дубликатов
            dup_df1 = pd.DataFrame(copy.deepcopy(origin_df[origin_df[name].isin(list(df[name]))]))
            dup_df2 = pd.DataFrame(copy.deepcopy(origin_df[origin_df[name].isin(list(df[name]))]))
            dup_df1['key'] = dup_df2['key'] = 1
            mrg = pd.merge(dup_df1, dup_df2, on='key', how='outer')
            mrg = mrg.drop('key', axis=1)
            duplicate_df = pd.concat([duplicate_df, mrg])

            # создаём таблицу выбранных значений
            try:
                choice_df.loc[len(choice_df.index), 'Choice'] = choice.values[0]
            except:
                e = sys.exc_info()[1]
                print(e.args[0], 173)
                print(choice)

            return

        for x in df[name].unique():
            res = df[df.apply(lambda row: fuzz.token_sort_ratio(str(row[name]), x), axis=1) >= min_likenesses[name]][
                df.columns[1:]]

            if len(res) > 1:
                worker(res)
            else:
                try:
                    dropable_ind.append(res.iloc[0]['Исходный Индекс'])
                except:
                    e = sys.exc_info()[1]
                    print(e.args[0], 187)
                    print(res)

        working_df = working_df[~working_df['Исходный Индекс'].isin(dropable_ind)]

    starter()
    return working_df


# заполняем DataFrame необходимыми данными
origin_df = filling_origin_df(df_dict)
working_df = copy.deepcopy(origin_df)


# чистим полученный DataFrame от лишнего
working_df = data_cleaner()


test1 = stupid_worker()

test2 = origin_df[~origin_df['Исходный Индекс'].isin(list(working_df['Исходный Индекс']))]
writer = pd.ExcelWriter('result.xlsx')
test2.to_excel(writer, 'clear data')

duplicate_df = duplicate_df.reset_index().drop('index', axis=1)
drop_list = list()
for ind, row in duplicate_df.iterrows():
    if row['Исходный Индекс_x'] == row['Исходный Индекс_y']:
        drop_list.append(ind)
duplicate_df = duplicate_df.drop(duplicate_df.index[drop_list]).reset_index()
duplicate_df.to_excel(writer, 'duplicates')

choice_df.to_excel(writer, 'choices')
writer.save()

print("--- %s seconds ---" % (time.time() - tic))
